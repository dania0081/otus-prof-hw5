﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Volodin_Hw_5
{
    public class CsvSerializer<T>
    {
        public string Serialize(T obj)
        {
            StringBuilder builder = new StringBuilder();
            var fields = obj.GetType().GetFields();

            foreach (FieldInfo property in fields)
            {
                builder.Append(property.GetValue(obj));

                if (property != fields.Last())
                {
                    builder.Append(",");
                }
            }

            return builder.ToString();
        }

        public T Deserialize(string str)
        {
            T obj = Activator.CreateInstance<T>();
            string[] values = str.Split(',');

            PropertyInfo[] properties = obj.GetType().GetProperties();

            for (int i = 0; i < properties.Length; i++)
            {
                properties[i].SetValue(obj, int.Parse(values[i]));
            }

            return obj;
        }
    }
}
