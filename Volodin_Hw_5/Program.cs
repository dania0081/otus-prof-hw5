﻿using System.Diagnostics;
using Newtonsoft.Json;

namespace Volodin_Hw_5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            CsvSerializer<F> serializer = new CsvSerializer<F>();
            Stopwatch stopwatch = new Stopwatch();

            Console.WriteLine($"---------------- CsvSerializer ----------------\n");
            F obj = F.Get();
            string csv = "";
            stopwatch.Start();
            for (int i = 0; i < 1000; i++)
            {
                csv = serializer.Serialize(obj);
            }
            stopwatch.Stop();
            Console.WriteLine($"CSV: {csv}");
            Console.WriteLine($"Время выполнения: {stopwatch.ElapsedMilliseconds} мс");
            stopwatch.Reset();
            F deserilizedObj = new F();
            stopwatch.Start();
            for (int i = 0; i < 1000; i++)
            {
                deserilizedObj = serializer.Deserialize(csv);
            }
            stopwatch.Stop();
            Console.WriteLine($"Deserialized: i1 = {deserilizedObj.i1}, i2 = {deserilizedObj.i2}, i3 = {deserilizedObj.i3}, i4 = {deserilizedObj.i4}, i5 = {deserilizedObj.i5}");
            Console.WriteLine($"Время выполнения: {stopwatch.ElapsedMilliseconds} мс");

            Console.WriteLine($"\n---------------- NewtonsoftJson ----------------\n");
            string json = "";
            stopwatch.Start();
            for (int i = 0; i < 1000; i++)
            {
                json = JsonConvert.SerializeObject(obj);
            }
            stopwatch.Stop();
            Console.WriteLine($"Json: {json}");
            Console.WriteLine($"Время выполнения: {stopwatch.ElapsedMilliseconds} мс");
            stopwatch.Start();
            for (int i = 0; i < 1000; i++)
            {
                deserilizedObj = JsonConvert.DeserializeObject<F>(json);
            }
            stopwatch.Stop();
            Console.WriteLine($"Deserialized: i1 = {deserilizedObj.i1}, i2 = {deserilizedObj.i2}, i3 = {deserilizedObj.i3}, i4 = {deserilizedObj.i4}, i5 = {deserilizedObj.i5}");
            Console.WriteLine($"Время выполнения: {stopwatch.ElapsedMilliseconds} мс");
        }

        class F
        {
            public int i1, i2, i3, i4, i5;

            public static F Get() => new F() 
            {
                i1 = 1,
                i2 = 2,
                i3 = 3,
                i4 = 4,
                i5 = 5 
            };
        }
    }
}